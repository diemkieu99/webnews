<!DOCTYPE html>
<html lang="en">
@include('page.common.head')
<body>
<!-- Topbar Start -->
@include('page.common.top')
<!-- Topbar End -->
@include('page.common.navbar')

@yield('content')

@include('page.common.footer')

</body>

</html>