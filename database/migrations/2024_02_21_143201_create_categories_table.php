<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('parent_id')->nullable()->default(0)->index()->comment('Danh mục cha');
            $table->string('slug')->unique();
            $table->string('banner')->nullable()->comment('Ảnh danh mục');
            $table->string('description')->nullable()->comment('Mô tả danh mục');
            $table->tinyInteger('show_home')->default(0)->comment('Hiển thị tại trang chủ : 1 hiển thị, 0 ẩn');
            $table->tinyInteger('status')->default(1)->comment('Trạng thái hiển thị : 1 hiển thị, 0 ẩn');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
