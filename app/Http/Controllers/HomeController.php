<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index()
    {
        return view('page.home.index');
    }

    public function category()
    {
        return view('page.category.index');
    }

    public function contact()
    {
        return view('page.contact.index');
    }

    public function single()
    {
        return view('page.single.index');
    }
}
